import Cardano from "../serialization-lib";
import { fromHex } from "../../utils/converter";
import WalletDict from "./walletDict";

class Wallet {
  async enable(name) {
    const wallet = (WalletDict)[name];

    if(wallet) {
      const instance = await wallet.enable();
      if (instance) {
        this._provider = instance;
        return true;
      }
    }
    return false;
  }

  async getBalance() {
    return await this._provider.getBalance();
  };

  async getCollateral() {
    //on cardano.nami only in 'experimental'
    return await this._provider.experimental.getCollateral();
  };

  async getNetworkId() {
    return await this._provider.getNetworkId();
  };

  async getUsedAddresses() {
    const usedAddresses = await this._provider.getUsedAddresses();

    return usedAddresses.map((address) =>
      Cardano.Instance.Address.from_bytes(fromHex(address)).to_bech32()
    );
  };

  async getUtxos() {
    return await this._provider.getUtxos();
  };

  async signTx(tx, partialSign = true) {
    return await this._provider.signTx(tx, partialSign);
  };

  async submitTx(tx) {
    return await this._provider.submitTx(tx);
  };
}

export default new Wallet();
