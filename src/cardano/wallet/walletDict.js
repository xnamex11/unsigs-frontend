const cardanoAvailable = () => {
    if(typeof window === 'undefined') return false;
    if(typeof window.cardano === 'undefined') return false;
    return true;
}

const WalletDict = {
    'Nami': cardanoAvailable() ? window.cardano.nami : {},
    'ccvault': cardanoAvailable() ? window.cardano.ccvault : {},
    'Flint': cardanoAvailable() ?  window.cardano.flint : {},
    'Yoroi': cardanoAvailable() ? window.cardano.yoroi : {},
    'CardWallet': cardanoAvailable() ? window.cardano.cardwallet : {},
    'GeroWallet': cardanoAvailable() ? window.cardano.gerowallet : {}
}

export default WalletDict
