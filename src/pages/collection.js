import React, { useState, useEffect } from "react"
import Cardano from "../cardano/serialization-lib"
import Wallet from "../cardano/wallet"
import { serializeTxUnspentOutput, valueToAssets } from "../cardano/transaction"
import { unsigPolicyId } from "../cardano/market-contract"
import { fromHex, toStr } from "../utils/converter"
import { useStoreActions, useStoreState } from "easy-peasy";

import { Unsig } from "../components/Unsig"
import { Box, Heading, Text, Flex } from "@chakra-ui/react"
import useWallet from "../hooks/useWallet"

function getWalletAssets(wallet) {
  const nativeAssets = wallet.utxos
    .map((utxo) => serializeTxUnspentOutput(utxo).output())
    .filter((txOut) => txOut.amount().multiasset() !== undefined)
    .map((txOut) => valueToAssets(txOut.amount()))
    .flatMap((assets) =>
      assets
        .filter((asset) => asset.unit !== "lovelace")
        .map((asset) => asset.unit)
    );

    return [...new Set(nativeAssets)];
  };

  function getMyUnsigs(wallet) {
    let assetList = getWalletAssets(wallet);
    let myNFTS = [];

    assetList.forEach(nft => {
      if (nft.startsWith(unsigPolicyId)) {
        let output = fromHex(nft.substring(56))
        let myWord = toStr(output)
        let myNumber = myWord.substring(5)
        myNFTS.push(myNumber)
    }
  });

  console.log(myNFTS);
  return myNFTS;
}

async function getAllMyAssets() {
  let assetList = await getWalletAssets();
  let myNFTS = [];

  assetList.forEach(nft => {
    console.log(nft);
    let output = fromHex(nft.substring(56))
    let myWord = toStr(output)
    console.log(myWord)
    myNFTS.push(myWord)
  });

  console.log(myNFTS);
  return myNFTS;
}

const CollectionPage = ({ unsigs }) => {
  const connected = useStoreState((state) => state.connection.connected);
  const [collection, setCollection] = useState([]);
  const ownedUnsigs = useStoreState((state) => state.ownedUnsigs.unsigIds);
  const setOwnedUnsigs = useStoreActions((actions) => actions.ownedUnsigs.add);
  const setOfferedUnsigs = useStoreActions((actions) => actions.myOffers.add);
  const [myOffers, setMyOffers] = useState([]);
  const { wallet } = useWallet(null);

  useEffect(() => {
    if (wallet) {
      const unsigs = getMyUnsigs(wallet);
      setCollection(unsigs);
      loadMyOffers();
    }
  }, [wallet]);

  useEffect(() => {
    if (collection) {
      setOwnedUnsigs(collection);
    }
  }, [collection]);

  const loadMyOffers = async () => {
    // offers endpoint is paginated, so this might not actually work
    // TODO - create new endpoint /api/v1/offersByOwner...
    const response = await fetch(`${process.env.GATSBY_MAINNET_API_URL}/offers?owner=${connected}`)
    const data = await response.json()
    // Above line will be removed after we call new endpoint
    setMyOffers(data);
    console.log("MY OFFERS", data.resultList)
  }

  useEffect(() => {
    setOfferedUnsigs(myOffers);
  }, [myOffers])


  return (
    <Box w='100%' minH='800px' px='24' py='12' bg='#232129' color='white'>
      <title>collection</title>
      <Box>
        <Heading size='4xl' fontWeight='medium'>
          my collection
        </Heading>
        {connected ? (
          <Box>
            { (myOffers.resultList?.length > 0) ? (
              <Box my='5'>
                <Heading py='5'>
                  For Sale:
                </Heading>
                <Flex direction='row' wrap='wrap'>
                  {myOffers?.resultList?.map((i) => <Unsig key={i} number={i.details.index} price={i.amount} />)}
                </Flex>
                { (collection.length > 0) ? (
                  <Heading py='5'>
                    Hodling:
                  </Heading>
                ) : ("")}
              </Box>
            ) : (
              <Box my='5'>
                <Text fontSize='2xl'>
                  Click on an unsig to create an offer.
                </Text>
              </Box>
            )}
            <Flex direction='row' wrap='wrap'>
              {collection.map((i) => <Unsig key={i} number={i} />)}
            </Flex>
            <Box py='10'>
              <Text fontSize='xl' py='3'>
                Wallet connected at address:
              </Text>
              <Heading fontSize='md' fontWeight='light'>
                {connected}
              </Heading>
            </Box>
          </Box>
        ) : (
          <div>
            <Text fontSize='xl' py='3'>
              To view your collection of unsigs, please connect a wallet.
            </Text>
          </div>
        )}

      </Box>

    </Box>
  )
}

export default CollectionPage