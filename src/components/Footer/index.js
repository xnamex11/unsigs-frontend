import { Flex, Text } from "@chakra-ui/react";
import React from "react";

const Footer = () => {
    return(
        <Flex id='footer' width='100%' py='10' justify='center' bg='#232129' color='#FFF'>
            <Text>A project of the Unsigned_DAO and Gimbalabs</Text>
        </Flex>
    )
}

export default Footer;