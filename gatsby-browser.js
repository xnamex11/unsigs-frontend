import CustomLayout from "./wrapPageElement";
import "./src/main.css";
import "@fontsource/poppins";
import "@fontsource/jetbrains-mono"; // Defaults to weight 400.

export const wrapPageElement = CustomLayout;